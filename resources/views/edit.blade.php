@extends('layout.master')
@section('title','edit')
@section('content')
    <h1 class="main-red">edit page</h1>

    <form action="{{ url('people',[$people->id]) }}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>firstname</label>
            <input type="text" class="form-control" name="fname" value="{{$people->fname}}">    
        </div>
        <div class="form-group">
                <label>lastname</label>
                <input type="text" class="form-control" name="lname" value="{{$people->lname}}">  
        </div>
        <div class="form-group">
                <label>age</label>
                <input type="text" class="form-control" name="age" value="{{$people->age}}">      
        </div>
        <button type="submit" class="btn btn-dark" style="margin:20px;">update</button>
     
        <button type="reset" value="Reset" class="btn btn-dark" >Reset</button>
        @if($errors->any())
            <div class="alert alert-danger ">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>

@endsection
