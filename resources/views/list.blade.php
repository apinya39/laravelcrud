@extends('layout.master')

@section('title','List')

@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css')}}">
@endsection


@section('content')

@if(Session::has('message'))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

<h1 class="main-red">list page</h1>
<div class="button" style="margin:20px;">
    <a href="{{ url('people/create')}}">
        <button type="button" class="btn btn-success">Create</button>
    </a>
</div>  
<table class="table table-dark">
    <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Lastname</th>
        <th>age</th>
        <th>create_date</th>
        <th>update_date</th>
        <th>Action</th>
    </thead>
    <tbody>
        @foreach ($people as $p)    
        <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->fname}}</td>
            <td>{{$p->lname}}</td>
            <td>{{$p->age}}</td>
            <td>{{ date('d-m-Y', strtotime($p->created_at)) }}</td>
            <td>{{ date('d-m-Y', strtotime($p->updated_at)) }}</td>
           
            <td>
                <div class="form-inline">
                    <div class="button" style="margin-right:10px;">
                        <a href="{{ url('people/'.$p->id.'/edit')}}" class="btn btn-primary">EDIT</a>
                    </div>
                        <form action="{{ url('people/'. $p->id)}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">DELETE</button>
                    </form>
                </div>
            </td>
            </td>
        </tr>
        @endforeach

    </tbody>
</table>

    
@endsection

@section('js')
   <script>
   
   
   </script>
@endsection