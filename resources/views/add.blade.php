@extends('layout.master')
@section('title','add')
@section('content')
    <h1 class="main-red">add page</h1>

    <form action="{{ url('people')}}" method="post">
        @csrf
        <div class="form-group">
            <label>firstname</label>
            <input type="text" class="form-control" name="fname">    
        </div>
        <div class="form-group">
                <label>lastname</label>
                <input type="text" class="form-control" name="lname">    
        </div>
        <div class="form-group">
                <label>age</label>
                <input type="text" class="form-control" name="age">    
        </div>
        <button type="submit" class="btn btn-dark" style="margin:20px;">save</button>
        @if($errors->any())
            <div class="alert alert-danger ">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>

@endsection
